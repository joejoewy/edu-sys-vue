import Vue from 'vue'
import Router from 'vue-router'
import Layout from '@/layout'

Vue.use(Router)

const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path*',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/',
    name: 'Login',
    hidden: true,
    component: () => import('@/views/login/index'),
    meta: { title: '登录' }
  },
  {
    path: '/dashboard',
    component: Layout,
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/index'),
        name: 'Dashboard',
        meta: { title: '主页', icon: 'home', affix: true }
      }
    ]
  },
  {
    path: '/user',
    component: Layout,
    name: 'UserPage',
    meta: {
      title: '用户管理',
      icon: 'users'
    },
    children: [
      {
        path: 'student/list',
        component: () => import('@/views/user/student/list'),
        name: 'UserStudentPageList',
        meta: { title: '学生列表', noCache: true }
      },
      {
        path: 'student/edit',
        component: () => import('@/views/user/student/edit'),
        name: 'UserStudentEdit',
        meta: { title: '学生编辑', noCache: true, activeMenu: '/user/student/list' },
        hidden: true
      },




      {
        path: 'admin/list',
        component: () => import('@/views/user/admin/list'),
        name: 'UserAdminPageList',
        meta: { title: '管理员列表', noCache: true }
      },
      {
        path: 'admin/edit',
        component: () => import('@/views/user/admin/edit'),
        name: 'UserAdminEdit',
        meta: { title: '管理员编辑', noCache: true, activeMenu: '/user/admin/list' },
        hidden: true
      },
      {
        path: 'teacher/list',
        component: () => import('@/views/user/teacher/list'),
        name: 'UserTeacherPageList',
        meta: { title: '老师列表', noCache: true }
      },
      {
        path: 'teacher/edit',
        component: () => import('@/views/user/teacher/edit'),
        name: 'UserTeacherEdit',
        meta: { title: '老师编辑', noCache: true, activeMenu: '/user/teacher/list' },
        hidden: true
      }
    ]
  },




  // 课程视频管理
  {
    path: '/course-video',
    component: Layout,
    name: 'CourseVideo',
    meta: {
      title: '课程视频管理',
      icon: 'course'
    },
    children: [
      {
        path: 'video/list',
        component: () => import('@/views/course-video/video/list'),
        name: 'CourseVideoList',
        meta: { title: '课程视频列表', noCache: true }
      },
      {
        path: 'video/edit',
        component: () => import('@/views/course-video/video/edit'),
        name: 'CourseVideoEdit',
        meta: { title: '课程创建', noCache: true, activeMenu: '/course-video/video/edit' },
      },
    ]
  },

  // 答卷管理相关

  {
    path: '/exam',
    component: Layout,
    name: 'ExamPage',
    hidden: true,
    meta: {
      title: '卷题管理',
      icon: 'exam'
    },
    children: [
      {
        path: 'paper/list',
        component: () => import('@/views/exam/paper/list'),
        name: 'ExamPaperPageList',
        meta: { title: '试卷列表', noCache: true }
      },
      {
        path: 'paper/edit',
        component: () => import('@/views/exam/paper/edit'),
        name: 'ExamPaperEdit',
        meta: { title: '试卷编辑', noCache: true, activeMenu: '/exam/paper/list' },
        hidden: true
      },
      {
        path: 'question/list',
        component: () => import('@/views/exam/question/list'),
        name: 'ExamQuestionPageList',
        meta: { title: '题目列表', noCache: true }
      },
      {
        path: 'question/edit/singleChoice',
        component: () => import('@/views/exam/question/edit/single-choice'),
        name: 'singleChoicePage',
        meta: { title: '单选题编辑', noCache: true, activeMenu: '/exam/question/list' },
        hidden: true
      },
      {
        path: 'question/edit/multipleChoice',
        component: () => import('@/views/exam/question/edit/multiple-choice'),
        name: 'multipleChoicePage',
        meta: { title: '多选题编辑', noCache: true, activeMenu: '/exam/question/list' },
        hidden: true
      },
      {
        path: 'question/edit/trueFalse',
        component: () => import('@/views/exam/question/edit/true-false'),
        name: 'trueFalsePage',
        meta: { title: '判断题编辑', noCache: true, activeMenu: '/exam/question/list' },
        hidden: true
      },

      {
        path: 'question/edit/gapFilling',
        component: () => import('@/views/exam/question/edit/gap-filling'),
        name: 'gapFillingPage',
        meta: { title: '填空题编辑', noCache: true, activeMenu: '/exam/question/list' },
        hidden: true
      },

      {
        path: 'question/edit/shortAnswer',
        component: () => import('@/views/exam/question/edit/short-answer'),
        name: 'shortAnswerPage',
        meta: { title: '简答题编辑', noCache: true, activeMenu: '/exam/question/list' },
        hidden: true
      }
    ]
  },



  {
    path: '/task',
    component: Layout,
    name: 'TaskPage',
    meta: {
      title: '作业管理',
      icon: 'task'
    },
    alwaysShow: true,
    children: [
      {
        path: 'list',
        component: () => import('@/views/task/list'),
        name: 'TaskListPage',
        meta: { title: '作业列表', noCache: true }
      },
      {
        path: 'edit',
        component: () => import('@/views/task/edit'),
        name: 'TaskEditPage',
        meta: { title: '创建作业', noCache: true }
      }
    ]
  },

  {
    path: '/book',
    component: Layout,
    name: 'BookPage',
    meta: {
      title: '书籍管理',
      icon: 'book'
    },
    alwaysShow: true,
    children: [
      {
        path: 'list',
        component: () => import('@/views/book/list'),
        name: 'BookListPage',
        meta: { title: '书籍列表', noCache: true }
      },
      {
        path: 'edit',
        component: () => import('@/views/book/edit'),
        name: 'BookEditPage',
        meta: { title: '书籍创编', noCache: true, activeMenu: '/book/list' }
      }
    ]
  },

  {
    path: '/class',
    component: Layout,
    name: 'ClassPage',
    meta: {
      title: '班级管理',
      icon: 'class'
    },
    alwaysShow: true,
    children: [
      {
        path: 'list',
        component: () => import('@/views/class/list'),
        name: 'classPage1',
        meta: { title: '班级列表', noCache: true }
      },
      {
        path: 'edit',
        component: () => import('@/views/class/edit'),
        name: 'classEditPage',
        meta: { title: '班级编辑', noCache: true, activeMenu: '/class/list' },
        hidden: true
      }
    ]
  },

  {
    path: '/education/subject',
    component: Layout,
    name: 'subjectPage',
    meta: {
      title: '学科管理',
      icon: 'subject'
    },
    alwaysShow: true,
    children: [
      {
        path: 'list',
        component: () => import('@/views/education/subject/list'),
        name: 'classPage1',
        meta: { title: '学科列表', noCache: true }
      },
      {
        path: 'edit',
        component: () => import('@/views/education/subject/edit'),
        name: 'classEditPage',
        meta: { title: '学科编辑', noCache: true, activeMenu: '/education/subject/list' },
        hidden: true
      }
    ]
  },



  {
    path: '/answer',
    component: Layout,
    name: 'AnswerPage',
    meta: {
      title: '成绩管理',
      icon: 'answer'
    },
    alwaysShow: true,
    children: [
      {
        path: 'list',
        component: () => import('@/views/answer/list'),
        name: 'AnswerPageList',
        meta: { title: '作业列表', noCache: true }
      },
      {
        path: 'edit',
        component: () => import('@/views/answer/edit'),
        name: 'RolePermissionList',
        meta: { title: '作业评分', noCache: true }
      }
    ]
  },
  {
    path: '/role',
    component: Layout,
    name: 'MessagePage',
    meta: {
      title: '角色管理',
      icon: 'role'
    },
    alwaysShow: true,
    children: [
      {
        path: 'list',
        component: () => import('@/views/role/list'),
        name: 'MessageListPage',
        meta: { title: '角色列表', noCache: true }
      },
      {
        path: 'edit',
        component: () => import('@/views/role/edit'),
        name: 'RoleAdd',
        meta: { title: '角色编辑', noCache: true }
      },
     
    ]
  },

  {
    path: '/permission',
    component: Layout,
    name: 'permissionPage',
    meta: {
      title: '权限管理',
      icon: 'permission'
    },
    alwaysShow: true,
    children: [
      {
        path: 'list',
        component: () => import('@/views/permission/list'),
        name: 'permissionPage1',
        meta: { title: '权限列表', noCache: true }
      },
      {
        path: 'edit',
        component: () => import('@/views/permission/edit'),
        name: 'permissionEditPage',
        meta: { title: '权限编辑', noCache: true, activeMenu: '/permission/list' },
        hidden: true
      }
    ]
  },
  {
    path: '/log',
    component: Layout,
    name: 'LogPage',
    meta: {
      title: '日志中心',
      icon: 'log'
    },
    alwaysShow: true,
    children: [
      {
        path: 'user/list',
        component: () => import('@/views/log/list'),
        name: 'LogUserPage',
        meta: { title: '用户日志', noCache: true }
      }
    ]
  },
  {
    path: '/rolePermissions',
    component: Layout,
    name: 'rolePermissions',
    meta: {
      title: '角色权限管理',
      icon: 'log'
    },
    alwaysShow: true,
    children: [
      {
        path: 'list',
        component: () => import('@/views/rolePermission/list'),
        name: 'RolePermissionList',
        meta: { title: '角色权限编辑', noCache: true }
      }
    ]
  },




  {
    path: '/profile',
    component: Layout,
    hidden: true,
    children: [
      {
        path: 'index',
        component: () => import('@/views/profile/index'),
        name: 'Profile',
        meta: { title: '个人简介', icon: 'user', noCache: true }
      }
    ]
  },
  {
    path: '*',
    hidden: true,
    component: () => import('@/views/error-page/404'),
    meta: { title: '404', noCache: true }
  }
]

const router = new Router({
  routes: constantRoutes
})

export {
  constantRoutes,
  router
}
