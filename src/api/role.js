import { post,get,put,deleteUser } from '@/utils/request'
export default {
    //查找
    List: query => get(`/role/${query.pageNum}/${query.pageSize}`, query),
    createRole: query => post(`/role`,query),
    updaterole: query => put(`/role`,query),
    selectRole: id =>get(`/role/` + id),
    deleteRole: id => deleteUser(`/role/${id}`),
    // send: query => post('/api/admin/message/send', query)
  }