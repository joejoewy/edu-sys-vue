import { post,get,put,deleteUser} from '@/utils/request'

export default {
  //查询学生
  getUserPageList: query => get(`/student/page/${query.pageNum}/${query.pageSize}`, query),
  //用户日志
  getUserEventPageList: query => get(`/log/page/${query.pageNum}/${query.pageSize}`, query),
  //新增
  createUser: query => post('/student', query),
  //根据id查找
  selectUser: id => get(`/student/` + id),
  //查询全部班级
  selectAllClass: ()=> get('/class'),
  //查询全部角色
  selectAllRole: ()=> get('/role'),
  getCurrentUser: () => post('/api/admin/user/current'),
  //修改
  updateUser: query => put('/student', query),
 //删除
  deleteUser: id => deleteUser(`/student/${id}`),
  //名字查询学生
  selectByUserName: query => get(`/student/page/${query.pageNum}/${query.pageSize}`, query),
}
