import { get, postWithLoadTip } from '@/utils/request'

export default {
   //登入
  login: query => postWithLoadTip(`/login/admin`, query),
  // 登出
  logout: query => get(`/login/logout`, query),
  //教师登录
  teacherLogin: query => postWithLoadTip(`/login/teacher`, query),

}
