import { post, get, put, deleteAdmin } from '@/utils/request'

export default {

  getTeacherPageList: query => get(`/teacher/page/${query.pageIndex}/${query.pageSize}` + '?keyword=' + query.keyword),
  changeStatus: id => get(`/teacher/page/1/5`+id),
  deleteTeacher:id=> deleteAdmin(`/teacher/${id}`),
  getTeacherPageByKeyword: query => get(`/teacher/real/${query.pageIndex}/${query.pageSize}`+'?keyword='+query.keyword),
  selectTeacher:id=> get(`/teacher/${id}`),
  createTeacher: query => post(`/teacher`,query),
  updateTeacher: query => put(`/teacher`,query),


  

}
