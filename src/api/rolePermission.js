import { post, get, put, deleteAdmin } from '@/utils/request'

export default {
  getPidsByRid: id => get(`/rolePermission/${id}`),
  updatePermission: query => put(`/rolePermission`,query)
}
