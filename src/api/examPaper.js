import { post, get, put, deleteHomeworkRecord} from '@/utils/request'

export default {
  pageList: query => post('/api/admin/exam/paper/page', query),
  findAll: query => get(`/homeworkRecord/findAll/${query.pageIndex}/${query.pageSize}`),
  findHomeworkRecordByHidSid: query => get(`/homeworkRecord/${query.sid}/${query.hid}`),
  findHomeworkRecordsBySid: query => get(`/homeworkRecord/page/${query.pageIndex}/${query.pageSize}` + "?sid=" + query.sid),
  findHomeWorkRecordByHid: query => get(`/homeworkRecord/page/${query.pageIndex}/${query.pageSize}/${query.hid}`),
  updateScore: query => put (`/homeworkRecord`,query),
  deleteHomeworkRecord: query => deleteHomeworkRecord('/homeworkRecord',query)
}
