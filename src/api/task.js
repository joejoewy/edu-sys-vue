import { post, get, deleteTask, put } from '@/utils/request'

export default {
  pageList: query => get(`/homework/page/${query.pageIndex}/${query.pageSize}`, query),
  edit: query => put('/homework/', query),
  add: query => post('/homework/', query),
  select: hid => get(`/homework/${hid}`),
  deleteTask: hid => deleteTask(`/homework/${hid}`)
}
