import { post, get, put, deleteVideo } from '@/utils/request'


export default {
    list: query => get(`/courseVideo/${query.pageIndex}/${query.pageSize}`, query),
    add: query => post('/courseVideo', query),
    edit: query => put('/courseVideo', query),
    select: vid => get(`/courseVideo/${vid}`),
    deleteVideo: id => deleteVideo(`/courseVideo/${id}`)
}