import { get, put, deleteBook } from '@/utils/request'

export default {
    list: query => get(`/book/page/${query.pageIndex}/${query.pageSize}`,query),
    edit: query => put('/book', query),
    select: bid => get(`/book/${bid}`),
    deleteBookByBid: bid => deleteBook(`/book/${bid}`)
}
