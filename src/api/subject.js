import { post, get, put, deleteSubject } from '@/utils/request'

export default {
  list: query => post('/api/admin/education/subject/list'),
  pageList: query => get(`/subject/page/${query.pageIndex}/${query.pageSize}`,),
  edit: query => put('/subject', query),
  select: id => get(`/subject/${id}`),
  deleteSubject: id => deleteSubject(`/subject/${id}`),
  addSubject: query => post(`/subject/`, query),

}
