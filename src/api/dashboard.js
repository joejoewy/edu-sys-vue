import { get } from '@/utils/request'

export default {
  index: () => get('/dashboard')
}
