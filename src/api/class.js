import { post, get, put, deleteSubject } from '@/utils/request'

export default {
    select: id => get(`/class/${id}`),
}
