import { post, get, put, deleteAdmin } from '@/utils/request'

export default {

  getAdminPageList:query=> get(`/admin/page/${query.pageIndex}/${query.pageSize}`),
  changeStatus:id=> get(`/admin/page/1/5`),
  deleteAdmin:id=> deleteAdmin(`/admin/${id}`),
  getAdminPageByKeyword:query=> get(`/admin/keyword/${query.keyword}/${query.pageIndex}/${query.pageSize}`),
  selectAdmin:id=> get(`/admin/${id}`),
  createAdmin:query=> post(`/admin`,query),
  updateAdmin:query=> put(`/admin/`,query),

}
